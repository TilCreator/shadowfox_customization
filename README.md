# My ShadowFox customization
![screenshot](screenshot.png)
## Install
* Install ShadowFox and actiavte it
* Clone this repo into `~/.mozilla/firefox/<your profile>/chrome/ShadowFox_customization`
* Run ShadowFox `shadowfox-updater -generate-uuids -set-dark-theme -profile-name <your profile>`
## Tree Style Tabs styling
```css
/* Add private browsing indicator per tab */
tab-item.private-browsing tab-label:before {
  content: "🕶 ";
}

tab-item.discarded .label-content {
  color: #fff;
}

tab-item.discarded .label-content:before {
  content: "💤 ";
  filter: grayscale(1);
}
```